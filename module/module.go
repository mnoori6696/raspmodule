package module

import (
	"strconv"
	"log"
	"time"

	"github.com/shanghuiyang/rpi-devices/dev"
	"gobot.io/x/gobot/drivers/gpio"
	"gobot.io/x/gobot/platforms/raspi"
)

const (
	LIGHT_SWITCH_PIN = 29
	PUMP_1_PIN       = 31
	PUMP_2_PIN       = 33
	PUMP_3_PIN       = 35
	PUMP_4_PIN       = 37
	SONIC_TRIG       = 20 // Board pin 38
	SONIC_ECHO       = 21 // Board pin 40
)

type RaspModule struct {
	sonicSensor      *dev.HCSR04
	lightBotton      *gpio.LedDriver
	WaterPump1Botton *gpio.LedDriver
	WaterPump2Botton *gpio.LedDriver
	WaterPump3Botton *gpio.LedDriver
	WaterPump4Botton *gpio.LedDriver
	LightState       bool
	WaterPump1State  bool
	WaterPump2State  bool
	WaterPump3State  bool
	WaterPump4State  bool
}

func InitializeModule() *RaspModule {
	adaptor := raspi.NewAdaptor()
	raspModule := &RaspModule{
		sonicSensor:      dev.NewHCSR04(SONIC_TRIG, SONIC_ECHO),
		lightBotton:      gpio.NewLedDriver(adaptor, strconv.Itoa(LIGHT_SWITCH_PIN)),
		WaterPump1Botton: gpio.NewLedDriver(adaptor, strconv.Itoa(PUMP_1_PIN)),
		WaterPump2Botton: gpio.NewLedDriver(adaptor, strconv.Itoa(PUMP_2_PIN)),
		WaterPump3Botton: gpio.NewLedDriver(adaptor, strconv.Itoa(PUMP_3_PIN)),
		WaterPump4Botton: gpio.NewLedDriver(adaptor, strconv.Itoa(PUMP_4_PIN)),
		LightState:       false, // Initial Light state is set to false (off)
		WaterPump1State:  false, // Inital Pump 1 set to false (off)
		WaterPump2State:  false, // Inital Pump 2 set to false (off)
		WaterPump3State:  false, // Inital Pump 3 set to false (off)
		WaterPump4State:  false, // Inital Pump 4 set to false (off)
	}
	raspModule.ToggleLightState(raspModule.LightState)
	raspModule.ToggleWaterPump(PUMP_1_PIN, raspModule.WaterPump1State)
	raspModule.ToggleWaterPump(PUMP_2_PIN, raspModule.WaterPump2State)
	raspModule.ToggleWaterPump(PUMP_3_PIN, raspModule.WaterPump3State)
	raspModule.ToggleWaterPump(PUMP_4_PIN, raspModule.WaterPump4State)
	return raspModule
}

func (rp *RaspModule) GetDistance() float64 {
	numRuns := 4
	total := float64(0)
	for i := 0; i < numRuns; i++ {
		d, err := rp.sonicSensor.Dist()
		if err != nil {
			log.Printf("failed to get distance, error: %+v", err)
			time.Sleep(100 * time.Millisecond)
			continue
		}
		total += d
		time.Sleep(250 * time.Millisecond)
	}
	return total / float64(numRuns)
}

func (rp *RaspModule) ToggleLightState(state bool) {
	togglePin(rp.lightBotton, state, "light switch")
}

func (rp *RaspModule) ToggleWaterPump(pumpNum int, state bool) {
	switch pumpNum {
	case PUMP_1_PIN:
		togglePin(rp.WaterPump1Botton, state, "water pump 1")
	case PUMP_2_PIN:
		togglePin(rp.WaterPump2Botton, state, "water pump 2")
	case PUMP_3_PIN:
		togglePin(rp.WaterPump3Botton, state, "water pump 3")
	case PUMP_4_PIN:
		togglePin(rp.WaterPump4Botton, state, "water pump 4")
	}
}

func togglePin(pinDriver *gpio.LedDriver, state bool, pinName string) {
	if state {
		err := pinDriver.On()
		if err != nil {
			log.Printf("failed to turn %s on", pinName)
		}
	} else {
		err := pinDriver.Off()
		if err != nil {
			log.Printf("failed to turn %s off", pinName)
		}
	}
}
