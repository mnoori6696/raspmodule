module raspberry_pi

go 1.16

require (
	github.com/bmizerany/pat v0.0.0-20210406213842-e4b6760bdd6f // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/donovanhide/eventsource v0.0.0-20210830082556-c59027999da0 // indirect
	github.com/eclipse/paho.mqtt.golang v1.3.5 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/godbus/dbus/v5 v5.0.6 // indirect
	github.com/gofrs/uuid v4.1.0+incompatible // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/muka/go-bluetooth v0.0.0-20211122080231-b99792bbe62a // indirect
	github.com/nats-io/nats.go v1.13.0 // indirect
	github.com/shanghuiyang/rpi-devices v0.0.0-20211124162610-0627a454e25a
	github.com/urfave/cli v1.22.5 // indirect
	github.com/veandco/go-sdl2 v0.4.10 // indirect
	go.bug.st/serial v1.3.3 // indirect
	gobot.io/x/gobot v1.15.0
	gocv.io/x/gocv v0.29.0 // indirect
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871 // indirect
	golang.org/x/net v0.0.0-20211123203042-d83791d6bcd9 // indirect
	golang.org/x/sys v0.0.0-20211124211545-fe61309f8881 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	periph.io/x/periph v3.6.8+incompatible // indirect
	tinygo.org/x/bluetooth v0.4.0 // indirect
)
