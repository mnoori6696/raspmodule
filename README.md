install gobot 

```bash
go get -d -u gobot.io/x/gobot/...
```

comment out V2 from the following files
```bash
vim ../../pkg/mod/github.com/gobuffalo/uuid@v2.0.5+incompatible/uuid.go
vim ../../pkg/mod/github.com/gobuffalo/uuid@v2.0.5+incompatible/uuid.goclear
```

Proto on raspberry pi
```bash
# Install
sudo apt update
sudo apt install snapd

sudo reboot

sudo snap install core

sudo snap install protobuf --classic

# Make proto file
# Run the following in root dir
protoc -I=. --go_out=. api.proto 
```


build the binary with
```bash
GOARM=6 GOARCH=arm GOOS=linux go build
```
