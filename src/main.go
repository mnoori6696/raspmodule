package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"os"
	"os/signal"
	api "raspberry_pi/api"
	module "raspberry_pi/module"
	"strconv"
	"syscall"
	"time"
)

const (
	PublishFrequency       = 3                       // Seconds
	Broker                 = "192.168.0.14"          // MQTT Broker adderess
	Port                   = 1883                    // MQTT Port
	MQTTPubTopic           = "RaspModule"            // MQTT Publish Topic
	MQTTSubTopicLight      = "RaspModule/Light"      // MQTT Subscribe Topic Light
	MQTTSubTopicWaterPump1 = "RaspModule/WaterPump1" // MQTT Subscribe Topic WaterPump1
	MQTTSubTopicWaterPump2 = "RaspModule/WaterPump2" // MQTT Subscribe Topic WaterPump2
	MQTTSubTopicWaterPump3 = "RaspModule/WaterPump3" // MQTT Subscribe Topic WaterPump3
	MQTTSubTopicWaterPump4 = "RaspModule/WaterPump4" // MQTT Subscribe Topic WaterPump4
)

var (
	connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
		fmt.Println("Connected\n")
	}
	connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
		fmt.Printf("Connect lost: %v\n", err)
	}
)

func initPubHandler(raspModule *module.RaspModule) mqtt.MessageHandler {
	return func(client mqtt.Client, msg mqtt.Message) {
		payLoad := string(msg.Payload())
		fmt.Printf("Received message: %s from topic: %s\n", payLoad, msg.Topic())
		val, err := strconv.Atoi(payLoad)
		if err != nil {
			fmt.Printf("Unrecognized value recieved for topic %s: message: %v (MUST BE INT 0 or 1)\n", MQTTSubTopicLight, payLoad)
			return
		}
		switch msg.Topic() {
		case MQTTSubTopicLight:
			if val == 0 || val == 1 {
				raspModule.ToggleLightState(val == 1)
			}

		case MQTTSubTopicWaterPump1:
			if val == 0 || val == 1 {
				raspModule.ToggleWaterPump(module.PUMP_1_PIN, val == 1)
			}

		case MQTTSubTopicWaterPump2:
			if val == 0 || val == 1 {
				raspModule.ToggleWaterPump(module.PUMP_2_PIN, val == 1)
			}

		case MQTTSubTopicWaterPump3:
			if val == 0 || val == 1 {
				raspModule.ToggleWaterPump(module.PUMP_3_PIN, val == 1)
			}

		case MQTTSubTopicWaterPump4:
			if val == 0 || val == 1 {
				raspModule.ToggleWaterPump(module.PUMP_4_PIN, val == 1)
			}
		}

	}
}

func sub(client mqtt.Client, raspModule *module.RaspModule) {
	token := client.Subscribe(MQTTSubTopicLight, 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to topic: %s\n", MQTTSubTopicLight)

	token = client.Subscribe(MQTTSubTopicWaterPump1, 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to topic: %s\n", MQTTSubTopicWaterPump1)

	token = client.Subscribe(MQTTSubTopicWaterPump2, 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to topic: %s\n", MQTTSubTopicWaterPump2)

	token = client.Subscribe(MQTTSubTopicWaterPump3, 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to topic: %s\n", MQTTSubTopicWaterPump3)

	token = client.Subscribe(MQTTSubTopicWaterPump4, 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to topic: %s\n", MQTTSubTopicWaterPump4)
}

func publishState(client mqtt.Client, raspModule *module.RaspModule) {
	tentAndLight := api.TentWaterAndLightNodeUpdate{
		WaterFullPercentage: float32(raspModule.GetDistance()),
		LightOn:             raspModule.LightState,
		WaterPumpAOn:        raspModule.WaterPump1State,
		WaterPumpBOn:        raspModule.WaterPump2State,
	}
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(tentAndLight)
	if err != nil {
		fmt.Printf("Failed encoding TentWaterAndLightNodeUpdate struct into bytes, err: %v\n", err)
		return
		// log.Fatal(err)
	}
	token := client.Publish("MQTTPubTopic", 0, false, tentAndLight)
	token.Wait()
	fmt.Printf("publishing: %v\n", tentAndLight)
}

func publish(client mqtt.Client, raspModule *module.RaspModule) {
	for {
		<-time.After(PublishFrequency * time.Second)
		go publishState(client, raspModule)
	}
}

func main() {
	raspModule := module.InitializeModule()
	raspModule.ToggleLightState(false)
	time.Sleep(1 * time.Second)
	raspModule.ToggleWaterPump(module.PUMP_1_PIN, true)
	raspModule.ToggleWaterPump(module.PUMP_2_PIN, true)
	raspModule.ToggleWaterPump(module.PUMP_3_PIN, true)
	raspModule.ToggleWaterPump(module.PUMP_4_PIN, true)

	// Make it more efficient
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", Broker, Port))
	opts.SetDefaultPublishHandler(initPubHandler(raspModule))
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	sub(client, raspModule)
	go publish(client, raspModule)
	// client.Disconnect(250)
	<-c
}
